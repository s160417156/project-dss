﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectDSS_AHP
{
    class AHP
    {
        #region Data Member
        private List<double[,]> listPV;
        private string[] alternatives;
        #endregion

        #region Property
        public List<double[,]> ListPV { get => listPV; set => listPV = value; }
        public string[] Alternatives { get => alternatives; set => alternatives = value; }
        #endregion

        #region Constructor
        public AHP(List<double[,]> _criteria, string[] _alternatives)
        {
            this.listPV = _criteria;
            this.alternatives = _alternatives;
        }
        #endregion

        #region Method
        public static double[,] CalcPriorityVector(double[,] matrixCriteria, bool withDetail = false)
        {
            // inisialisasi panjang dan lebar matrix
            int x = (matrixCriteria.GetLength(0) * 2) + 2;
            int y = matrixCriteria.GetLength(0) + 1;
            double[,] priorityVector = new double[x, y];
            
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    // masukin data dari matriks perbandingan ke matriks PV
                    if (i < matrixCriteria.GetLength(0))
                    {
                        if (j != y - 1)
                        {
                            priorityVector[i, j] = matrixCriteria[i, j];
                        }
                        else
                        {
                            double total = 0;
                            for (int k = 0; k < matrixCriteria.GetLength(1); k++)
                            {
                                total += matrixCriteria[i, k];
                            }
                            priorityVector[i, j] = total;
                        }
                    }
                    // membagi isi matrik perbandingan semula dengan jumlah kolom yang bersesuaian
                    else if (i < matrixCriteria.GetLength(0) * 2)
                    {
                        if (j != y - 1)
                        {
                            priorityVector[i, j] = (matrixCriteria[(i - matrixCriteria.GetLength(0)), j]) / priorityVector[(i - matrixCriteria.GetLength(0)), priorityVector.GetLength(1) - 1];
                        }
                        else
                        {
                            priorityVector[i, j] = 1;
                        }
                    }
                    // menghitung jumlah
                    else if (i < (matrixCriteria.GetLength(0) * 2) + 1)
                    {
                        if (j != y - 1)
                        {
                            double total = 0;
                            for (int k = 0; k < matrixCriteria.GetLength(1); k++)
                            {
                                total += priorityVector[(k + matrixCriteria.GetLength(1)), j];
                            }
                            priorityVector[i, j] = total;
                        }
                        else
                        {
                            priorityVector[i, j] = priorityVector.GetLength(1) - 1;
                        }
                    }
                    // menghitung priority vector
                    else
                    {
                        if (j != y - 1)
                        {
                            priorityVector[i, j] = priorityVector[i - 1, j] / priorityVector[i - 1, priorityVector.GetLength(1) - 1];
                        }
                        else
                        {
                            priorityVector[i, j] = 1;
                        }
                    }
                }
            }

            if (withDetail)
            {
                return priorityVector;
            }
            else
            {
                double[,] onlyPV = new double[1, y];
                for (int i = 0; i < priorityVector.GetLength(1) - 1; i++)
                {
                    onlyPV[0, i] = priorityVector[priorityVector.GetLength(0) - 1, i];
                }
                return onlyPV;
            }
        }

        public static bool isPVConsistance(double[,] priorityVector, out string[] detail)
        {
            // cek apakah matriks PV yang dimasukkan lengkap dengan matriks perbandingannya
            if (priorityVector.GetLength(0) > 1)
            {
                // isi tabel RI dari 0 - 15
                double[] tableRI = new double[16] { 0, 0, 0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49, 1.51, 1.48, 1.56, 1.57, 1.59 };

                // pisahkan PV sama hasil sum dari matriks perbandingan (coporation matrix (cm))
                // untuk mempermudah penghitungan
                double[] pv = new double[priorityVector.GetLength(1) - 1], cm = new double[priorityVector.GetLength(1) - 1];

                for (int i = 0; i < priorityVector.GetLength(1) - 1; i++)
                {
                    pv[i] = priorityVector[priorityVector.GetLength(0) - 1, i];
                }

                int realSize = (priorityVector.GetLength(0) - 2) / 2;
                for (int i = 0; i < priorityVector.GetLength(0); i++)
                {
                    if (i < realSize)
                    {
                        cm[i] = priorityVector[i, realSize];
                    }
                    else
                    {
                        break;
                    }
                }

                // cek konsistensi
                double lambdaMax = 0;
                for (int i = 0; i < pv.Length; i++)
                {
                    for (int j = 0; j < cm.Length; j++)
                    {
                        if (i == j)
                        {
                            lambdaMax += (cm[j] * pv[i]);
                        }
                    }
                }
                double ci = (lambdaMax - realSize) / (realSize - 1);
                double ri = tableRI[realSize];
                double cr = ci / ri;

                detail = new string[4] { "λmax : " + lambdaMax, "CI   : " + ci, "RI   : " + ri, "CR   : " + cr };

                if (cr < 0.1) return true;
                else return false;
            }
            else
            {
                // kalau isinya cuma ada pv aja, proses dibatalkan
                throw new Exception("Isi matriks tidak lengkap, pengecekan konsistensi PV harus dilengkapi dengan matriks perbandingan.");
            }
        }

        public List<double[]> ListOnlyPV()
        {
            List<double[]> pvArray = new List<double[]>();

            for (int i = 0; i < ListPV.Count; i++)
            {
                double[] pv = new double[ListPV[i].GetLength(1) - 1];

                for (int j = 0; j < ListPV[i].GetLength(1) - 1; j++)
                {
                    pv[j] = ListPV[i][ListPV[i].GetLength(0) - 1, j];
                }

                pvArray.Add(pv);
            }

            return pvArray;
        }

        public List<double> CalcOverallPriority()
        {
            List<double> overallPriority = new List<double>();

            int t = 2;
            string testing = "";
            for (int x = ListPV[1].GetLength(0) - 1; x < ListPV[1].GetLength(0); x++)
            {
                for (int y = 0; y < ListPV[1].GetLength(1) - 1; y++)
                {
                    string viewData = "";
                    double countOverall = 0;
                    t = ListPV[0].GetLength(1) - 1;
                    for (int i = 1; i < ListPV.Count; i++)
                    {
                        if (t == ListPV[0].GetLength(1) - 1)
                        {
                            t = 0;
                            countOverall = countOverall + (ListPV[0][ListPV[0].GetLength(0) - 1, t] * ListPV[i][ListPV[i].GetLength(0) - 1, y]);
                        }
                        else
                        {
                            t++;
                            countOverall = countOverall + (ListPV[0][ListPV[0].GetLength(0) - 1, t] * ListPV[i][ListPV[i].GetLength(0) - 1, y]);
                        }
                    }

                    overallPriority.Add(countOverall);
                }
            }

            return overallPriority;
        }

        public string[] RankAHP(List<double> op)
        {
            List<string[]> rank = new List<string[]>();
            for (int n = 0; n < Alternatives.Length; n++)
            {
                double best = 0;
                string bestAlternatif = "";

                for (int i = 0; i < op.Count; i++)
                {
                    if (best < op[i])
                    {
                        bool isThere = false;
                        for (int j = 0; j < rank.Count; j++)
                        {
                            if (op[i].ToString() == rank[j][0])
                            {
                                isThere = true;
                            }
                        }

                        if (!isThere)
                        {
                            best = op[i];
                            bestAlternatif = Alternatives[i];
                        }
                    }
                }

                string[] nominee = new string[] { best.ToString(), bestAlternatif };
                rank.Add(nominee);
            }

            string[] ranking = new string[rank.Count];
            for (int i = 0; i < rank.Count; i++)
            {
                ranking[i] = rank[i][1];
            }

            return ranking;
        }

        public string BestAHP(List<double> op)
        {
            double bestOf1 = 0;
            string bestAlternatif_1 = "";

            for (int i = 0; i < op.Count; i++)
            {
                if (bestOf1 < op[i])
                {
                    bestOf1 = op[i];
                    bestAlternatif_1 = Alternatives[i];
                }
            }

            return bestAlternatif_1;
        }
        #endregion
    }
}
