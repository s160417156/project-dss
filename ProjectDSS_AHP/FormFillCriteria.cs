﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectDSS_AHP
{
    public partial class FormFillCriteria : Form
    {
        public FormFillCriteria()
        {
            InitializeComponent();
        }

        #region Global Variable
        FormHome home;
        int space = 5;
        int dimention = 0;
        string now = "";
        #endregion

        #region Control Method
        private void FormFillCriteria_Load(object sender, EventArgs e)
        {
            // isi form-nya dihapus semua dulu
            this.Controls.Clear();

            home = (FormHome)Owner;
            now = home.matrixNow;

            // inisialisasi dimensi berdasarkan kriteria atau alternatif
            if (now == "main")
            {
                dimention = home.criterias.Length;
            }
            else if (now == "each")
            {
                dimention = home.alternatives.Length;
            }

            // generate textBox sebanyak dimensi
            // (misal: dimensinya 3 berarti textBox dibuat sebanyak 3 x 3)
            for (int i = 1; i <= dimention; i++)
            {
                for (int j = 1; j <= dimention; j++)
                {
                    TextBox tb = new TextBox();
                    tb.Name = "tbMatrix" + i + j;
                    tb.KeyDown += new KeyEventHandler(tb_KeyDown); // event KeyDown textBox
                    tb.Location = new Point(space * i + (100 * ( i - 1)), space * j + (20 * (j - 1)));
                    if (i == j)
                    {
                        tb.Text = "1";
                        tb.Enabled = false;
                    }

                    this.Controls.Add(tb);
                }
            }

            // generate button Selesai di bawah textBox
            Button b = new Button();
            b.Name = "bFill";
            b.Text = "Selesai";
            b.Location = new Point(space, space + (space * dimention + (20 * dimention)));
            b.Size = new Size((space * dimention + (100 * dimention)) - space, 20);
            b.Click += new EventHandler(bFill_Click); // event Click button
            this.Controls.Add(b);

            // inisialisasi ukuran form sesuai banyaknya textBox
            this.Size = new Size(space * dimention + (100 * dimention) + space, space * (dimention + 1) + (20 * (dimention + 1)) + space + 2);
        }

        // event Click button Selesai
        private void bFill_Click(object sender, EventArgs e)
        {
            home = (FormHome)Owner;

            // buat array 2D sebesar dimensi yang sudah ditentukan
            double[,] criteria = new double[dimention, dimention];

            // ngecek cara pengisian matriksnya udah bener atau belum
            // kalau udah bener langsung masukin ke array
            // kalau salah langsung dibatalin proses selanjutnya
            bool isLegal = true;
            for (int i = 1; i <= dimention; i++)
            {
                for (int j = 1; j <= dimention; j++)
                {
                    try
                    {
                        // karena textBox-nya generate otomatis, bukan buatan manual
                        // cara manggilnya gini:
                        TextBox tb = (TextBox)this.Controls.Find("tbMatrix" + i + j, true)[0];

                        if (tb.Text == "")
                        {
                            // kalau textBox-nya kosong, cek textBox yang lokasinya inverse
                            // dari textBox yang sekarang lagi dicek
                            TextBox tbInverse = (TextBox)this.Controls.Find("tbMatrix" + j + i, true)[0];
                            if (tbInverse.Text != "")
                            {
                                // isi array posisi sekarang dengan pembagian dari (1 / textBox yang lokasinya inverse)
                                // method Replace itu buat ganti titik (.) jadi koma (,) karena pemisah desimalnya koma (antisipasi user ngisinya titik)
                                //criteria[i - 1, j - 1] = Math.Round(1 / double.Parse(tbInverse.Text.Replace('.', ',')), home.MaxLength() - 2);
                                criteria[i - 1, j - 1] = Math.Round(1 / double.Parse(tbInverse.Text), home.MaxLength() - 2);
                            }
                            else
                            {
                                // kalau textBox yang lokasinya inverse ternyata kosong juga, proses langsung dibatalin
                                MessageBox.Show("Pola pengisian bidang tidak sesuai.", "Tidak Sesuai", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                isLegal = false;
                                break;
                            }
                        }
                        else
                        {
                            // isi array posisi sekarang dengan isi textBox yang dicek sekarang
                            //criteria[i - 1, j - 1] = double.Parse(tb.Text.Replace('.', ','));
                            criteria[i - 1, j - 1] = double.Parse(tb.Text);
                        }
                    }
                    catch (Exception x)
                    {
                        MessageBox.Show("Data yang dimasukkan harus berupa angka semua.", "Tidak Sesuai", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        isLegal = false;
                        break;
                    }
                }
                if (!isLegal) { break; }
            }

            if (isLegal)
            {
                // hitung PV
                double[,] pv = new double[(dimention * 2) + 2, dimention + 1];
                pv = AHP.CalcPriorityVector(criteria, true);
                bool cekkonsis = AHP.isPVConsistance(pv, out string[] detail);

                // proses di bawah ini untuk nampilkan ke textBox
                if (now == "main")
                {
                    home.lbPreview.Items.Add("Priority Vector");
                    home.lbPreview.Items.Add("===============");
                    home.lbPreview.Items.Add("");
                }

                // nampilin border dan header tabel di listBox
                home.Border();
                home.Header();
                home.Border();

                // nampilin isi array ke listBox (i baris j kolom)
                for (int i = 1; i <= dimention + 1; i++)
                {
                    string line = "| ";

                    if (i == dimention + 1)
                    {
                        home.Border();
                    }

                    // j-nya dimensi tambah 1, soalnya kolom pertama buat nampilin tiap kriterianya
                    for (int j = 1; j <= (dimention * 2) + 2 + 1; j++)
                    {
                        // j pertama nampilin nama kriteria
                        if (j == 1)
                        {
                            // matriks perbandingan kolom pertama
                            // untuk tabel kriteria utama yang ditampilkan nama tiap kriteria
                            // untuk tabel tiap kriteria yang ditampilkan nama tiap alternatif
                            if (i != dimention + 1 )
                            {
                                if (now == "main")
                                {
                                    line += home.criterias[i - 1].PadRight(home.MaxLength(), ' ') + " | ";
                                }
                                else if (now == "each")
                                {
                                    line += home.alternatives[i - 1].PadRight(home.MaxLength(), ' ') + " | ";
                                }
                            }
                            else
                            {
                                line += "".PadRight(home.MaxLength(), ' ') + " | ";
                            }
                        }
                        else
                        {
                            // j selain yang pertama nampilin isi array
                            line += Math.Round(pv[j - 2, i - 1], home.MaxLength() - 2).ToString().PadRight(home.MaxLength(), ' ') + " | ";
                        }
                    }
                    home.lbPreview.Items.Add(line);
                }
                
                home.Border();
                home.lbPreview.Items.Add("");

                if (now == "main")
                {
                    // kalau matriks perbandingan yang diisi untuk kriteria utama
                    // matikan button Isi Kriteria Utama dan nyalain button Isi Kriteria Spesifik
                    // ganti teks button Isi Kriteria Spesifik sesuai dengan matriks perbandingan kriteria yang mau di isi nanti
                    home.bFillMainCriteria.Enabled = false;
                    home.bFillEachCriteria.Enabled = true;
                    home.bFillEachCriteria.Text = "Isi Kriteria " + home.criterias[home.pointer] + " dan PV";
                    home.bFillEachCriteria.Focus();
                    home.bMainPV.Enabled = false;
                    home.bEachPV.Enabled = true;
                    home.pointer++;
                }
                else if (now == "each")
                {
                    // kalau sudah semua kriteria diisi matriks perbandingannya,
                    // nyalain button untuk ngitung AHP-nya
                    if (home.pointer == home.criterias.Length)
                    {
                        home.bFillEachCriteria.Enabled = false;
                        home.bEachPV.Enabled = false;
                        home.bCalc.Enabled = true;
                        home.radioButtonBest1.Enabled = true;
                        home.radioButtonBest2.Enabled = true;
                        home.bCalc.Focus();
                    }
                    else
                    {
                        home.bFillEachCriteria.Text = "Isi Kriteria: " + home.criterias[home.pointer] + " dan PV";
                        home.pointer++;
                    }
                }

                // Untuk menampilkan detail cek konsistensi

                home.lbPreview.Items.Add("Detail Perhitungan Uji Konsistensi : ");

                for (int i = 0; i < detail.Length; i++)
                {
                    home.lbPreview.Items.Add(detail[i]);
                }

                if (cekkonsis == true)
                {
                    home.matrixes.Add(pv);
                    home.lbPreview.Items.Add("Matrix Konsisten karena hasil CR < 0,1");
                }
                else
                {
                    MessageBox.Show("Matrix yang digunakan tidak konsisten.", "Tidak Konsisten", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    home.lbPreview.Items.Add("Matrix Tidak Konsisten karena hasil CR >= 0,1");
                    home.radioButtonBest1.Enabled = false;
                    home.radioButtonBest2.Enabled = false;
                    home.bFillMainCriteria.Enabled = false;
                    home.bFillEachCriteria.Enabled = false;
                    home.bMainPV.Enabled = false;
                    home.bEachPV.Enabled = false;
                    home.bCalc.Enabled = false;
                }

                home.lbPreview.Items.Add("");
                Close();
            }
        }

        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
        #endregion
    }
}
