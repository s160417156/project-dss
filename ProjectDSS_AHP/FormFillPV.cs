﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectDSS_AHP
{
    public partial class FormFillPV : Form
    {
        public FormFillPV()
        {
            InitializeComponent();
        }

        #region Global Variable
        FormHome home;
        int space = 5;
        int dimention = 0;
        string now = "";
        #endregion

        #region Control Method
        private void FormFillPV_Load(object sender, EventArgs e)
        {
            // isi form-nya dihapus semua dulu
            this.Controls.Clear();

            home = (FormHome)Owner;
            now = home.matrixNow;

            // inisialisasi dimensi berdasarkan kriteria atau alternatif
            if (now == "pvmain")
            {
                dimention = home.criterias.Length;
            }
            else if (now == "pveach")
            {
                dimention = home.alternatives.Length;
            }

            // generate textBox sebanyak dimensi
            // (misal: dimensinya 3 berarti textBox dibuat sebanyak 3)
            for (int i = 1; i <= dimention; i++)
            {
                TextBox tb = new TextBox();
                tb.Name = "tbPV" + i;
                tb.Location = new Point(space, space * i + (20 * (i - 1)));
                tb.KeyDown += new KeyEventHandler(tb_KeyDown); // event KeyDown textBox
                tb.TextChanged += new EventHandler(tb_TextChanged);  // event TextChanged textBox
                this.Controls.Add(tb);
            }

            // generate label Total di bawah textBox
            Label l = new Label();
            l.Text = "-- Total --";
            l.AutoSize = false;
            l.TextAlign = ContentAlignment.MiddleCenter;
            l.Location = new Point(space, space + (space * dimention + (20 * dimention)));
            l.Size = new Size(100, 20);
            this.Controls.Add(l);

            // generate textBox Total PV di bawah textBox
            TextBox tbTotal = new TextBox();
            tbTotal.Name = "tbTotal";
            tbTotal.Text = "0";
            tbTotal.Enabled = false;
            tbTotal.Location = new Point(space, space + (space * dimention + (20 * dimention)) + 20 + space);
            tbTotal.Size = new Size(100, 20);
            this.Controls.Add(tbTotal);

            // generate button Selesai di bawah textBox
            Button b = new Button();
            b.Name = "bFill";
            b.Text = "Selesai";
            b.Location = new Point(space, space + (space * dimention + (20 * dimention)) + ((20 + space) * 2));
            b.Size = new Size(100, 20);
            b.Click += new EventHandler(bFill_Click); // event Click button
            this.Controls.Add(b);

            // inisialisasi ukuran form sesuai banyaknya textBox
            this.Size = new Size(space + 100 + space, space + (20 * dimention) + (space * dimention) + (20 * 3) + (space * 3));

        }

        // event TextChanged textBox tiap PV
        private void tb_TextChanged(object sender, EventArgs e)
        {
            // cek apakah semua textBox berisi angka
            double total = 0;
            bool isNumber = true;
            for (int i = 1; i <= dimention; i++)
            {
                TextBox tb = (TextBox)this.Controls.Find("tbPV" + i, true)[0];
                if (tb.Text != "")
                {
                    // kalau iya tambahkan variabel total dengan nilai dari textBox yang sedang dicek
                    double PV = 0;
                    //isNumber = double.TryParse(tb.Text.Replace('.', ','), out PV);
                    isNumber = double.TryParse(tb.Text, out PV);
                    total += PV;

                    if (total >= 0.999)
                    {
                        total = Math.Round(total, 0);
                    }

                    // kalau tidak proses langsung seketika dibatalkan
                    if (!isNumber)
                    {
                        MessageBox.Show("PV harus diisi dengan angka semua.", "Tidak Sesuai", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                }
            }

            // variabel total dipindah ke textBox tbTotal
            TextBox tbTotal = (TextBox)this.Controls.Find("tbTotal", true)[0];
            tbTotal.Text = total.ToString();
            // kalau ada kesalahan ubah isi textBox tbTotal jadi "Terjadi Kesalahan"
            if (!isNumber)
            {
                tbTotal.Text = "Terjadi Kesalahan";
            }
        }

        // event Click button Selesai
        private void bFill_Click(object sender, EventArgs e)
        {
            TextBox tbTotal = (TextBox)this.Controls.Find("tbTotal", true)[0];
            if (tbTotal.Text == "Terjadi Kesalahan")
            {
                MessageBox.Show("Proses tidak dapat dilanjutkan karena tidak semua PV yang dimasukkan berupa angka.", "Tidak Sesuai", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (tbTotal.Text != "1")
            {
                MessageBox.Show("Proses tidak dapat dilanjutkan karena jumlah PV harus sama dengan 1.", "Tidak Sesuai", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                home = (FormHome)Owner;

                // PV dari textBox dimasukkan ke array
                double[,] pv = new double[1, dimention + 1];
                for (int i = 1; i <= dimention + 1; i++)
                {
                    if (i != dimention + 1)
                    {
                        TextBox tb = (TextBox)this.Controls.Find("tbPV" + i, true)[0];
                        if (tb.Text == "")
                        {
                            tb.Text = "0";
                        }
                        //pv[0, i - 1] = Math.Round(double.Parse(tb.Text.Replace('.', ',')), home.MaxLength() - 2);
                        pv[0, i - 1] = Math.Round(double.Parse(tb.Text), home.MaxLength() - 2);
                    }
                    else
                    {
                        pv[0, i - 1] = 1;
                    }
                }

                // tambahkan PV ke matriks
                home.matrixes.Add(pv);


                // proses di bawah ini untuk nampilkan ke textBox
                if (now == "pvmain")
                {
                    home.lbPreview.Items.Add("Priority Vector");
                    home.lbPreview.Items.Add("===============");
                    home.lbPreview.Items.Add("");
                }

                // nampilin border dan header tabel di listBox
                home.Border();
                home.Header();
                home.Border();

                // nampilin isi array ke listBox (i baris j kolom)
                for (int i = 1; i <= dimention + 1; i++)
                {
                    string line = "| ";

                    if (i == dimention + 1)
                    {
                        home.Border();
                    }

                    // j-nya dimensi tambah 1, soalnya kolom pertama buat nampilin tiap kriterianya
                    for (int j = 1; j <= 2; j++)
                    {
                        // j pertama nampilin nama kriteria
                        if (j == 1)
                        {
                            // matriks PV kolom pertama
                            // untuk tabel kriteria utama yang ditampilkan nama tiap kriteria
                            // untuk tabel tiap kriteria yang ditampilkan nama tiap alternatif
                            if (i != dimention + 1)
                            {
                                if (now == "pvmain")
                                {
                                    line += home.criterias[i - 1].PadRight(home.MaxLength(), ' ') + " | ";
                                }
                                else if (now == "pveach")
                                {
                                    line += home.alternatives[i - 1].PadRight(home.MaxLength(), ' ') + " | ";
                                }
                            }
                            else
                            {
                                line += "".PadRight(home.MaxLength(), ' ') + " | ";
                            }
                        }
                        else
                        {
                            // j selain yang pertama nampilin isi array
                            //line += Math.Round(pv[j - 2, i - 1], home.MaxLength() - 2).ToString().Replace('.', ',').PadRight(home.MaxLength(), ' ') + " | ";
                            line += Math.Round(pv[j - 2, i - 1], home.MaxLength() - 2).ToString().PadRight(home.MaxLength(), ' ') + " | ";
                        }
                    }
                    home.lbPreview.Items.Add(line);
                }
                home.Border();
                home.lbPreview.Items.Add("");

                if (now == "pvmain")
                {
                    // kalau matriks perbandingan yang diisi untuk kriteria utama
                    // matikan button Isi Kriteria Utama dan nyalain button Isi Kriteria Spesifik
                    // ganti teks button Isi Kriteria Spesifik sesuai dengan matriks perbandingan kriteria yang mau di isi nanti
                    home.bFillMainCriteria.Enabled = false;
                    home.bFillEachCriteria.Enabled = true;
                    home.bFillEachCriteria.Text = "Isi Kriteria " + home.criterias[home.pointer] + " dan PV";
                    home.bFillEachCriteria.Focus();
                    home.bMainPV.Enabled = false;
                    home.bEachPV.Enabled = true;
                    home.pointer++;
                }
                else if (now == "pveach")
                {
                    // kalau sudah semua kriteria diisi matriks perbandingannya,
                    // nyalain button untuk ngitung AHP-nya
                    if (home.pointer == home.criterias.Length)
                    {
                        home.bFillEachCriteria.Enabled = false;
                        home.bEachPV.Enabled = false;
                        home.radioButtonBest1.Enabled = true;
                        home.radioButtonBest2.Enabled = true;
                        home.bCalc.Enabled = true;
                        home.bCalc.Focus();
                    }
                    else
                    {
                        home.bFillEachCriteria.Text = "Isi Kriteria: " + home.criterias[home.pointer] + " dan PV";
                        home.pointer++;
                    }
                }

                Close();
            }
        }

        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
        #endregion

    }
}