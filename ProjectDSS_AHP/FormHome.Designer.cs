﻿namespace ProjectDSS_AHP
{
    partial class FormHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbInitialize = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAlternative = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCriteria = new System.Windows.Forms.TextBox();
            this.bStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbMatrix = new System.Windows.Forms.GroupBox();
            this.bEachPV = new System.Windows.Forms.Button();
            this.bMainPV = new System.Windows.Forms.Button();
            this.bFillMainCriteria = new System.Windows.Forms.Button();
            this.bFillEachCriteria = new System.Windows.Forms.Button();
            this.bCalc = new System.Windows.Forms.Button();
            this.bReset = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbPreview = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonBest2 = new System.Windows.Forms.RadioButton();
            this.radioButtonBest1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.gbInitialize.SuspendLayout();
            this.gbMatrix.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbInitialize
            // 
            this.gbInitialize.Controls.Add(this.label3);
            this.gbInitialize.Controls.Add(this.tbTitle);
            this.gbInitialize.Controls.Add(this.label4);
            this.gbInitialize.Controls.Add(this.tbAlternative);
            this.gbInitialize.Controls.Add(this.label2);
            this.gbInitialize.Controls.Add(this.tbCriteria);
            this.gbInitialize.Controls.Add(this.bStart);
            this.gbInitialize.Controls.Add(this.label1);
            this.gbInitialize.Location = new System.Drawing.Point(12, 12);
            this.gbInitialize.Name = "gbInitialize";
            this.gbInitialize.Size = new System.Drawing.Size(391, 150);
            this.gbInitialize.TabIndex = 0;
            this.gbInitialize.TabStop = false;
            this.gbInitialize.Text = "Inisialisasi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Alternatif:";
            // 
            // tbAlternative
            // 
            this.tbAlternative.Location = new System.Drawing.Point(66, 92);
            this.tbAlternative.Name = "tbAlternative";
            this.tbAlternative.Size = new System.Drawing.Size(319, 20);
            this.tbAlternative.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kriteria:";
            // 
            // tbCriteria
            // 
            this.tbCriteria.Location = new System.Drawing.Point(66, 66);
            this.tbCriteria.Name = "tbCriteria";
            this.tbCriteria.Size = new System.Drawing.Size(319, 20);
            this.tbCriteria.TabIndex = 4;
            // 
            // bStart
            // 
            this.bStart.Location = new System.Drawing.Point(6, 119);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(379, 23);
            this.bStart.TabIndex = 7;
            this.bStart.Text = "Mulai";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.bStart_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(376, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pisahkan tiap kriteria dan alternatif dengan titik koma ( ; )";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbMatrix
            // 
            this.gbMatrix.Controls.Add(this.bEachPV);
            this.gbMatrix.Controls.Add(this.bMainPV);
            this.gbMatrix.Controls.Add(this.bFillMainCriteria);
            this.gbMatrix.Controls.Add(this.bFillEachCriteria);
            this.gbMatrix.Location = new System.Drawing.Point(12, 168);
            this.gbMatrix.Name = "gbMatrix";
            this.gbMatrix.Size = new System.Drawing.Size(391, 76);
            this.gbMatrix.TabIndex = 1;
            this.gbMatrix.TabStop = false;
            this.gbMatrix.Text = "Matriks Perbandingan dan PV";
            // 
            // bEachPV
            // 
            this.bEachPV.Enabled = false;
            this.bEachPV.Location = new System.Drawing.Point(265, 45);
            this.bEachPV.Name = "bEachPV";
            this.bEachPV.Size = new System.Drawing.Size(120, 23);
            this.bEachPV.TabIndex = 3;
            this.bEachPV.Text = "Langsung Isi PV";
            this.bEachPV.UseVisualStyleBackColor = true;
            this.bEachPV.Click += new System.EventHandler(this.bEachPV_Click);
            // 
            // bMainPV
            // 
            this.bMainPV.Enabled = false;
            this.bMainPV.Location = new System.Drawing.Point(265, 16);
            this.bMainPV.Name = "bMainPV";
            this.bMainPV.Size = new System.Drawing.Size(120, 23);
            this.bMainPV.TabIndex = 2;
            this.bMainPV.Text = "Langsung Isi PV";
            this.bMainPV.UseVisualStyleBackColor = true;
            this.bMainPV.Click += new System.EventHandler(this.bMainPV_Click);
            // 
            // bFillMainCriteria
            // 
            this.bFillMainCriteria.Enabled = false;
            this.bFillMainCriteria.Location = new System.Drawing.Point(6, 16);
            this.bFillMainCriteria.Name = "bFillMainCriteria";
            this.bFillMainCriteria.Size = new System.Drawing.Size(253, 23);
            this.bFillMainCriteria.TabIndex = 0;
            this.bFillMainCriteria.Text = "Isi Kriteria Utama dan PV";
            this.bFillMainCriteria.UseVisualStyleBackColor = true;
            this.bFillMainCriteria.Click += new System.EventHandler(this.bFillMainCriteria_Click);
            // 
            // bFillEachCriteria
            // 
            this.bFillEachCriteria.Enabled = false;
            this.bFillEachCriteria.Location = new System.Drawing.Point(6, 45);
            this.bFillEachCriteria.Name = "bFillEachCriteria";
            this.bFillEachCriteria.Size = new System.Drawing.Size(253, 23);
            this.bFillEachCriteria.TabIndex = 1;
            this.bFillEachCriteria.Text = "Isi Kriteria Spesifik dan PV";
            this.bFillEachCriteria.UseVisualStyleBackColor = true;
            this.bFillEachCriteria.Click += new System.EventHandler(this.bFillEachCriteria_Click);
            // 
            // bCalc
            // 
            this.bCalc.Enabled = false;
            this.bCalc.Location = new System.Drawing.Point(6, 42);
            this.bCalc.Name = "bCalc";
            this.bCalc.Size = new System.Drawing.Size(379, 23);
            this.bCalc.TabIndex = 0;
            this.bCalc.Text = "Hitung AHP";
            this.bCalc.UseVisualStyleBackColor = true;
            this.bCalc.Click += new System.EventHandler(this.bCalc_Click);
            // 
            // bReset
            // 
            this.bReset.Location = new System.Drawing.Point(6, 71);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(379, 23);
            this.bReset.TabIndex = 1;
            this.bReset.Text = "Reset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbPreview);
            this.groupBox1.Location = new System.Drawing.Point(409, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(842, 342);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pratinjau";
            // 
            // lbPreview
            // 
            this.lbPreview.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPreview.FormattingEnabled = true;
            this.lbPreview.Location = new System.Drawing.Point(6, 16);
            this.lbPreview.Name = "lbPreview";
            this.lbPreview.Size = new System.Drawing.Size(830, 316);
            this.lbPreview.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonBest2);
            this.groupBox2.Controls.Add(this.radioButtonBest1);
            this.groupBox2.Controls.Add(this.bCalc);
            this.groupBox2.Controls.Add(this.bReset);
            this.groupBox2.Location = new System.Drawing.Point(12, 250);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(391, 104);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Proses";
            // 
            // radioButtonBest2
            // 
            this.radioButtonBest2.AutoSize = true;
            this.radioButtonBest2.Enabled = false;
            this.radioButtonBest2.Location = new System.Drawing.Point(79, 19);
            this.radioButtonBest2.Name = "radioButtonBest2";
            this.radioButtonBest2.Size = new System.Drawing.Size(65, 17);
            this.radioButtonBest2.TabIndex = 3;
            this.radioButtonBest2.TabStop = true;
            this.radioButtonBest2.Text = "Ranking";
            this.radioButtonBest2.UseVisualStyleBackColor = true;
            // 
            // radioButtonBest1
            // 
            this.radioButtonBest1.AutoSize = true;
            this.radioButtonBest1.Checked = true;
            this.radioButtonBest1.Enabled = false;
            this.radioButtonBest1.Location = new System.Drawing.Point(12, 19);
            this.radioButtonBest1.Name = "radioButtonBest1";
            this.radioButtonBest1.Size = new System.Drawing.Size(61, 17);
            this.radioButtonBest1.TabIndex = 2;
            this.radioButtonBest1.TabStop = true;
            this.radioButtonBest1.Text = "Terbaik";
            this.radioButtonBest1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Judul:";
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(66, 19);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(319, 20);
            this.tbTitle.TabIndex = 1;
            // 
            // FormHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 364);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbMatrix);
            this.Controls.Add(this.gbInitialize);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aplikasi Pengambilan Keputusan Metode AHP";
            this.Load += new System.EventHandler(this.FormHome_Load);
            this.gbInitialize.ResumeLayout(false);
            this.gbInitialize.PerformLayout();
            this.gbMatrix.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbInitialize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbAlternative;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbCriteria;
        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbMatrix;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Button bFillEachCriteria;
        public System.Windows.Forms.Button bFillMainCriteria;
        public System.Windows.Forms.ListBox lbPreview;
        public System.Windows.Forms.Button bCalc;
        public System.Windows.Forms.Button bEachPV;
        public System.Windows.Forms.Button bMainPV;
        public System.Windows.Forms.RadioButton radioButtonBest2;
        public System.Windows.Forms.RadioButton radioButtonBest1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbTitle;
    }
}

