﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ProjectDSS_AHP
{
    public partial class FormHome : Form
    {
        public FormHome()
        {
            InitializeComponent();
        }

        #region Global Variable
        // inisialisasi variabel
        public int pointer = 0;
        public string matrixNow;
        public string[] criterias, alternatives;
        public List<double[,]> matrixes = new List<double[,]>();
        #endregion

        #region Method Control
        private void FormHome_Load(object sender, EventArgs e)
        {
            // mengaktifkan horizontal scrollbar untuk listBox Pratinjau
            lbPreview.HorizontalScrollbar = true;

            // awal buka otomatis fokus ke text box Kriteria
            tbTitle.Focus();
        }

        private void bStart_Click(object sender, EventArgs e)
        {
            // cek apakah semua textBox terisi
            if (tbTitle.Text != "" && tbCriteria.Text != "" && tbAlternative.Text != "")
            {
                // tempel judul dan garis bawah di listBox
                string title = tbTitle.Text;
                string titleBorder = "".PadRight(tbTitle.TextLength, '=');
                lbPreview.Items.Add(title);
                lbPreview.Items.Add(titleBorder);
                lbPreview.Items.Add("");

                // misah semua isi dalam textBox berdasarkan titik koma (;), isinya nanti jadi array
                criterias = tbCriteria.Text.Split(';');
                alternatives = tbAlternative.Text.Split(';');

                // proses nampilkan kriteria dan alternatif ke listBox biar rapi
                string firstLine = "Kriteria  : ", secondLine = "Alternatif: ";

                int i = 0;
                foreach (string criteria in criterias)
                {
                    firstLine += criteria; i++;
                    if (i != criterias.Length) firstLine += ", ";
                }
                
                i = 0;
                foreach (string alternative in alternatives)
                {
                    secondLine += alternative; i++;
                    if (i != alternatives.Length) secondLine += ", ";
                }

                lbPreview.Items.Add(firstLine);
                lbPreview.Items.Add(secondLine);
                lbPreview.Items.Add("");

                // matikan groupBox Inisialisasi, nyalain button Kriteria Utama
                gbInitialize.Enabled = false;
                bFillMainCriteria.Enabled = true;
                bMainPV.Enabled = true;
                bFillMainCriteria.Focus();
            }
            else
            {
                tbTitle.Focus();
                MessageBox.Show("Semua bidang wajib diisi.", "Ada Bidang Kosong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bFillMainCriteria_Click(object sender, EventArgs e)
        {
            // ngasih tau form buat isi matriks kalau matriks yang diisi
            // untuk inisialisasi matrix perbandingan kriteria utama
            matrixNow = "main";

            // buka form untuk isi matriks perbandingan
            FormFillCriteria form = new FormFillCriteria();
            form.Owner = this;
            form.ShowDialog();
        }

        private void bFillEachCriteria_Click(object sender, EventArgs e)
        {
            // ngasih tau form buat isi matriks kalau matriks yang diisi
            // untuk inisialisasi matrix perbandingan tiap-tiap kriteria
            matrixNow = "each";

            // buka form untuk isi matriks perbandingan
            FormFillCriteria form = new FormFillCriteria();
            form.Owner = this;
            form.ShowDialog();
        }

        private void bMainPV_Click(object sender, EventArgs e)
        {
            // ngasih tau form buat isi matriks kalau matriks yang diisi
            // untuk inisialisasi PV kriteria utama
            matrixNow = "pvmain";

            // buka form untuk isi matriks PV
            FormFillPV form = new FormFillPV();
            form.Owner = this;
            form.ShowDialog();
        }

        private void bEachPV_Click(object sender, EventArgs e)
        {
            // ngasih tau form buat isi matriks kalau matriks yang diisi
            // untuk inisialisasi pv tiap-tiap kriteria
            matrixNow = "pveach";

            // buka form untuk isi matriks PV
            FormFillPV form = new FormFillPV();
            form.Owner = this;
            form.ShowDialog();
        }

        private void bCalc_Click(object sender, EventArgs e)
        {
            matrixNow = "ov";

            AHP ahp = new AHP(matrixes, alternatives);

            lbPreview.Items.Add("Overall Priority");
            lbPreview.Items.Add("================");
            lbPreview.Items.Add("");

            List<double[]> pvs = ahp.ListOnlyPV();
            List<double> op = ahp.CalcOverallPriority();

            Border();
            Header();

            string line = "| " + "".PadRight(MaxLength(), ' ') + " | ";
            for (int i = 0; i < pvs[0].Length; i++)
            {
                line += Math.Round(pvs[0][i], MaxLength() - 2).ToString().PadRight(MaxLength(), ' ') + " | ";
            }
            line += "Total".PadRight(MaxLength(), ' ') + " | ";
            lbPreview.Items.Add(line);

            Border();

            for (int i = 0; i < ahp.Alternatives.Length; i++)
            {
                line = "| " + ahp.Alternatives[i].PadRight(MaxLength(), ' ') + " | ";

                for (int j = 1; j < pvs.Count; j++)
                {
                    line += Math.Round(pvs[j][i], MaxLength() - 2).ToString().PadRight(MaxLength(), ' ') + " | ";
                }

                line += Math.Round(op[i], MaxLength() - 2).ToString().PadRight(MaxLength(), ' ') + " | ";

                lbPreview.Items.Add(line);
            }


            Border();

            lbPreview.Items.Add("");
            lbPreview.Items.Add("Summary");
            lbPreview.Items.Add("=======");

            if (radioButtonBest1.Checked)
            {
                lbPreview.Items.Add("Pilihan Terbaik = " + ahp.BestAHP(op));
            }
            else
            {
                int rank = 1;
                string[] ranking = ahp.RankAHP(op);
                foreach (string nominee in ranking)
                {
                    lbPreview.Items.Add("Ranking " + rank + " = " + nominee);
                    rank++;
                }
            }

            lbPreview.Items.Add("");
        
            //List<string> rank = ahp.BestAHP(op, "top1");
        }

        private void bReset_Click(object sender, EventArgs e)
        {
            gbInitialize.Enabled = true;
            bFillMainCriteria.Enabled = false;
            bFillEachCriteria.Enabled = false;
            bEachPV.Enabled = false;
            bMainPV.Enabled = false;
            radioButtonBest1.Enabled = false;
            radioButtonBest2.Enabled = false;
            radioButtonBest1.Checked = true;
            bCalc.Enabled = false;
            lbPreview.Items.Clear();

            tbTitle.Text = "";
            tbCriteria.Text = "";
            tbAlternative.Text = "";
            bFillEachCriteria.Text = "Isi Kreteria Spesifik dan PV";

            pointer = 0;
            matrixes = new List<double[,]>();

            tbTitle.Focus();
        }
        #endregion

        // semua method di sini cuma buat rapihin bentuk tabel aja
        #region Additional Method

        // method untuk ngecek kata terpanjang dari tiap kriteria dan alternatif
        public int MaxLength()
        {
            int length = 6;

            foreach (string criteria in criterias)
            {
                if (length <= criteria.Length)
                {
                    length = criteria.Length;
                }
            }

            if (matrixNow.Contains("each"))
            {
                foreach (string alternative in alternatives)
                {
                    if (length <= alternative.Length)
                    {
                        length = alternative.Length;
                    }
                }
            }
            
            return length;
        }

        // bikin garis baris tabel
        public void Border()
        {
            string border = "---";
            //for (int i = 0; i <= MaxLength(); i++) { border += "-"; }
            border += "".PadRight(MaxLength(), '-') + "-";

            if (matrixNow == "main")
            {
                for (int i = 0; i < criterias.Length * 2 + 2; i++)
                {
                    border += "".PadLeft(MaxLength(), '-') + "---";
                }
            }
            else if (matrixNow == "each")
            {
                for (int i = 0; i < alternatives.Length * 2 + 2; i++)
                {
                    border += "".PadLeft(MaxLength(), '-') + "---";
                }
            }
            else if (matrixNow.Contains("pv"))
            {
                border += "".PadLeft(MaxLength(), '-') + "---";
            }
            else if (matrixNow == "ov")
            {
                for (int i = 0; i < criterias.Length; i++)
                {
                    border += "".PadLeft(MaxLength(), '-') + "---";
                }
                border += "".PadRight(MaxLength(), '-') + "---";
            }
            lbPreview.Items.Add(border);
        }

        // bikin header tabel
        public void Header()
        {
            string header = "| ";

            if (matrixNow.Contains("each"))
            {
                header += criterias[pointer - 1].PadRight(MaxLength(), ' ') + " ";
            }
            else
            {
                header += "".PadRight(MaxLength(), ' ') + " ";
            }

            int idx = 0;
            header += "| ";

            if (matrixNow == "main")
            {
                for (int i = 0; i < 2; i++)
                {
                    foreach (string criteria in criterias)
                    {
                        header += criteria.PadRight(MaxLength(), ' ') + " | ";
                        idx++;
                    }
                }
                header += "Jumlah".PadRight(MaxLength(), ' ') + " | ";
                header += "PV".PadRight(MaxLength(), ' ') + " | ";
            }
            else if (matrixNow == "each")
            {
                for (int i = 0; i < 2; i++)
                {
                    foreach (string alternative in alternatives)
                    {
                        header += alternative.PadRight(MaxLength(), ' ') + " | ";
                        idx++;
                    }
                }
                header += "Jumlah".PadRight(MaxLength(), ' ') + " | ";
                header += "PV".PadRight(MaxLength(), ' ') + " | ";
            }
            else if (matrixNow.Contains("pv"))
            {
                header += "PV".PadRight(MaxLength(), ' ') + " | ";
            }
            else if (matrixNow == "ov")
            {
                foreach (string criteria in criterias)
                {
                    header += criteria.PadRight(MaxLength(), ' ') + " | ";
                    idx++;
                }
                header += "".PadRight(MaxLength(), ' ') + " ";
                header += "| ";
            }

            lbPreview.Items.Add(header);
        }
        #endregion

    }
}
